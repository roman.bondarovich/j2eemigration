package com.softeq.jm;

import br.com.caelum.vraptor.VRaptor;

import javax.servlet.DispatcherType;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

@WebFilter(
    filterName = "vraptor",
    urlPatterns = {"/*"},
    dispatcherTypes = {DispatcherType.FORWARD, DispatcherType.REQUEST},
    asyncSupported = true
)
public class DefaultVRaptorFilter extends VRaptor {

    private static final Set<String> SPRING_PATHS = Collections.unmodifiableSet(new HashSet<>(Arrays.asList(
        "/home/home",
        "/"
//        "/company/companies"
    )));

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) req;
        String contextPath = request.getContextPath();
        String requestURI = request.getRequestURI();
        String path = requestURI.substring(contextPath.length());

        if (SPRING_PATHS.contains(path)) {
            chain.doFilter(req, res);
        } else {
            super.doFilter(req, res, chain);
        }

    }
}
