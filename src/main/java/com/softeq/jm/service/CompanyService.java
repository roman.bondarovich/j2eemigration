package com.softeq.jm.service;

import com.softeq.jm.SpringContextHolder;
import com.softeq.jm.model.Company;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Stateless
public class CompanyService {

    @PersistenceContext
    private EntityManager em;

//    @PostConstruct
//    private void addToSpringContext(){
//        ApplicationContext context = SpringContextHolder.getContext();
//        ConfigurableListableBeanFactory bf = ((ConfigurableApplicationContext) context).getBeanFactory();
//        bf.registerSingleton("companyService", this);
//    }

    public void persist(Company company) {
        em.persist(company);
    }

    public List<Company> findAll() {
        return em.createQuery("select c from Company c order by c.id").getResultList();
    }

}
