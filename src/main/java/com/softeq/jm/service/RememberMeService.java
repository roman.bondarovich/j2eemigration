package com.softeq.jm.service;

import com.softeq.jm.SpringContextHolder;
import com.softeq.jm.commons.core.AppUser;
import com.softeq.jm.commons.util.CookieUtil;
import com.softeq.jm.model.RememberMe;
import com.softeq.jm.repository.RememberMeRepository;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;
import java.util.UUID;

@Stateless
public class RememberMeService {

    private static final String COOKIE_NAME = "RememberMeCookie";

    @Inject
    private RememberMeRepository repo;

    @Inject
    private HttpServletResponse response;

    @PostConstruct
    private void addToSpringContext(){
        ApplicationContext context = SpringContextHolder.getContext();
        ConfigurableListableBeanFactory bf = ((ConfigurableApplicationContext) context).getBeanFactory();
        bf.registerSingleton("rememberMeService", this);
    }

    public void rememberUser(AppUser appUser) {
        String token = UUID.randomUUID().toString();
        save(appUser.getId(), token);
        CookieUtil.addCookie(response, COOKIE_NAME, token, 604800);
    }

    private void save(Integer userId, String token) {
        RememberMe rememberMe = repo.findByuser(userId);
        if (rememberMe == null) {
            rememberMe = new RememberMe();
            rememberMe.setUserId(userId);
        }
        rememberMe.setToken(token);
        repo.save(rememberMe);
    }
}
