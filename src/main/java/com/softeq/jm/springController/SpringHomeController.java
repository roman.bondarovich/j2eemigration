package com.softeq.jm.springController;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

@RestController
@RequestMapping
public class SpringHomeController {

    @GetMapping
    public ModelAndView index(){
        return  new ModelAndView("redirect:/home/home");
    }

    @GetMapping(value = "/home/home")
    public ModelAndView getHomePage(){
        ModelAndView home = new ModelAndView("/home/home");

        return home;
    }

}
