package com.softeq.jm.springController;

import com.softeq.jm.SpringContextHolder;
import com.softeq.jm.model.Company;
import com.softeq.jm.service.CompanyService;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/company")
public class SpringCompanyController {

    @Resource(mappedName = "java:global/jm/CompanyService!com.softeq.jm.service.CompanyService")
    private CompanyService companyService;

    @GetMapping("/companies")
    public ModelAndView getAllCompanies(){
        List<Company> companies = companyService.findAll();
        ModelAndView view = new ModelAndView("/company/companiesSpring");
        view.addObject("companiesSpring", companies);

        return view;
    }

    @PostMapping("/add")
    public ModelAndView addCompany(@Valid Company company){
        companyService.persist(company);
        return new ModelAndView("redirect:/company/companies");
    }


}
