package com.softeq.jm;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class SpringContextHolder {

    private static ApplicationContext context;

    @Autowired
    public SpringContextHolder(ApplicationContext context) {
        this.context = context;
    }

    public static ApplicationContext getContext() {

        return context;
    }

    public static void setContext(ApplicationContext context) {
        SpringContextHolder.context = context;
    }
}
